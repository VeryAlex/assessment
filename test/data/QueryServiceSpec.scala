package data

import models.{Airport, Country, Runway}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.scalatest.{AsyncWordSpec, BeforeAndAfter, Inspectors, Matchers}

import scala.concurrent.Future

class QueryServiceSpec extends AsyncWordSpec with Matchers with Inspectors with MockitoSugar with BeforeAndAfter {

  val dataService = mock[DataSource]

  before{
    val countryDummy1 = Country(1,"MX","Mexico","NA","dummy")
    val countryDummy2 = Country(2,"NL","NetherLands","EU","dummy")
    val airportDummy1 = Airport(1,"1","dummy","dummy",1,2,None,"NA","MX","dummy",None,"yes",None,None,None,None,None,None)
    val airportDummy2 = Airport(2,"2","dummy","dummy",1,2,None,"NA","NL","dummy",None,"yes",None,None,None,None,None,None)
    val airportDummy3 = Airport(3,"3","dummy","dummy",1,2,None,"NA","NL","dummy",None,"yes",None,None,None,None,None,None)
    val runWays1 = Runway(1,2,"1",None,None,None,true,false,None,None,None,None,None,None,None,None,None,None,None,None)
    val runWays2 = Runway(1,2,"2",None,None,None,true,false,None,None,None,None,None,None,None,None,None,None,None,None)
    val runWays3 = Runway(1,2,"3",None,None,None,true,false,None,None,None,None,None,None,None,None,None,None,None,None)


    when(dataService.getCountryByCode("Invalid")) thenReturn Future(None)

    when(dataService.getCountryByCode("MX")) thenReturn Future(Some(countryDummy1))
    when(dataService.getCountryByCode("NL")) thenReturn Future(Some(countryDummy2))

    when(dataService.getAirports) thenReturn Future(Seq(airportDummy1,airportDummy2,airportDummy3))

    when(dataService.getAirportsByCountry("MX")) thenReturn Future(Seq(airportDummy1))
    when(dataService.getAirportsByCountry("NL")) thenReturn Future(Seq(airportDummy2,airportDummy3))

    when(dataService.getCountryCodes) thenReturn Future(Seq("MX","NL"))

    when(dataService.getCountry("Mex")) thenReturn Future(Some(countryDummy1))
    when(dataService.getCountry("Mexico")) thenReturn Future(Some(countryDummy1))

    when(dataService.getCountries) thenReturn Future(Seq(countryDummy1,countryDummy2))

    when(dataService.getAllRunways) thenReturn Future(Seq(runWays1,runWays2,runWays3))

    when(dataService.getRunway("MX")) thenReturn Future(Seq(runWays1))
    when(dataService.getRunway("NL")) thenReturn Future(Seq(runWays2,runWays3))


  }
  val ds = new QueryReportServiceImpl(dataService)

  "Query service " should {

    "return a None object when querying airports and runways with an invalid code " in {
      val futureResult = ds.getQueryResultsByCode("Invalid")

      futureResult map { noneObject => noneObject shouldBe empty }
    }

    "return a query result with all airports and runways of a country when querying by code" in {
      val futureResult = ds.getQueryResultsByCode("MX")

      futureResult map {
        queryResultOpt =>

          queryResultOpt should not be empty

          val queryResult = queryResultOpt.get

          queryResult.code shouldBe "MX"

          queryResult.airports should not be empty
          queryResult.airports.size shouldBe 1
      }

    }

    "return a query result with all airports and runways of a country when querying by name" in {
      val futureResult = ds.getQueryResultsByName("Mex")

      futureResult map {
        queryResultOpt =>

          queryResultOpt should not be empty

          val queryResult = queryResultOpt.get

          queryResult.code shouldBe "MX"

          queryResult.airports should not be empty
          queryResult.airports.size shouldBe 1

      }
    }

    "return a Report for a certain number of records" in {
      val requested = 2
      val futureResult = ds.getReport(requested)

      futureResult map {
        reportOpt =>
          reportOpt should not be empty
          val report = reportOpt.get

          report.highest should have size requested
          report.lowest should have size requested

          report.highest.reverse shouldBe sorted
          report.lowest shouldBe sorted

          report.runwayTypes should have size 2

          val surfacesPerCountry = report.runwayTypes.map(_.uniqueSurfaces)

          forAll(surfacesPerCountry) {
            countrySurfaces =>
              countrySurfaces.toSet should have size countrySurfaces.size //Checking if there are no duplicates
          }

      }
    }
  }
}
