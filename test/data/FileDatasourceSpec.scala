package data

import org.scalatest.{AsyncWordSpec, Inspectors, Matchers}

/**
  * Created by alejandrohernandez on 8/21/17.
  */
class FileDatasourceSpec extends AsyncWordSpec with Matchers with Inspectors{


  "FileDataSource " should {
    val ds = new FileDataSource

    "return a list of countries when queried" in {
      val futureResult = ds.getCountries

      futureResult map {
        countries =>
          countries should not be empty
          countries should have size 247
      }
    }

    "return a list of countries codes when queried" in {
      val futureResult = ds.getCountryCodes
      futureResult map {
        countriesCodes =>
          countriesCodes should not be empty
          countriesCodes should have size 247
      }
    }

    "return a country when full name is entered" in {
      val futureResult = ds.getCountry("Mexico")

      futureResult map {
        country  =>
          country should not be empty
          country.get.code shouldBe "MX"
      }
    }

    "return a country when only the partial name is entered" in {
      val futureResult = ds.getCountry("Mex")

      futureResult map {
        country  =>
          country should not be empty
          country.get.code shouldBe "MX"
      }
    }

    "return a None object when querying a non existing country" in {
      val futureResult = ds.getCountry("Non existing country name")

      futureResult map { country  => country shouldBe empty }
    }

    "return a valid country by country code" in {
      val futureResult = ds.getCountryByCode("MX")

      futureResult map {
        country  =>
          country should not be empty
          country.get.code shouldBe "MX"
      }
    }

    "return a None object when querying a country by an invalid country code" in {
      val futureResult = ds.getCountryByCode("Non existing country code")

      futureResult map {country  => country shouldBe empty}
    }

    "return all airports of a country " in {
      val futureResult = ds.getAirportsByCountry("MX")

      futureResult map {
        airports  =>
          airports should not be empty
          val countryCodes = airports.map(_.isoCountry)
          forAll(countryCodes) {
            countryCode => countryCode shouldBe "MX"
          }
      }
    }

    "return an empty list for invalid country codes" in {
      val futureResult = ds.getAirportsByCountry("Invalid country code")

      futureResult map {airports => airports shouldBe empty}
    }

    "return all runways of a country " in {
      val futureResult = ds.getRunway("MX")
      val futureResult2 = ds.getAirportsByCountry("MX")

      futureResult2 flatMap {
        airports =>{
          futureResult map {
            runways  =>
              val airPortIds = airports.map(_.idEnt)
              runways should not be empty
              val airPortIdsInRunways = runways.map(_.airportIdent)
              airPortIds should contain allElementsOf airPortIdsInRunways
          }
        }
      }
    }

  }
}

