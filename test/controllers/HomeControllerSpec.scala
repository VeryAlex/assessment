package controllers

import data.{DataSource, QueryReportServiceI}
import models.Country
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfter
import org.scalatest.mockito.MockitoSugar
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.test._
import play.api.test.Helpers._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class HomeControllerSpec extends PlaySpec with GuiceOneAppPerTest with Injecting with MockitoSugar with BeforeAndAfter {
  val dataService = mock[DataSource]

  before {
    when(dataService.getCountryCodes) thenReturn Future(Seq("MX","NL"))
  }
  "HomeController GET" should {

    "render the index page from a new instance of controller" in {
      val controller = new HomeController(stubControllerComponents(),dataService)

      val result = controller.index.apply(FakeRequest(GET, "/"))

      //How to test UI? Wish I knew

    }

  }
}
