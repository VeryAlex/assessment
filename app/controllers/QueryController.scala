package controllers

import javax.inject._

import data.{DataSource, QueryReportServiceI}
import play.api.libs.json.Json
import play.api.mvc._
import play.api.data.Forms._
import play.api.data.Form

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Controller for the query option
  **/
@Singleton
class QueryController @Inject()(cc: ControllerComponents, reportService:QueryReportServiceI)
  extends AbstractController(cc) {

  /**
    * A method for handling querying via code
    **/
  def queryByCode() = Action.async { implicit request: Request[AnyContent] =>
    form.bindFromRequest.fold(
      errors =>{
        Future{
          BadRequest(s"Error while receiving from Form")
        }
      },
        data =>{
          val resultsF = reportService.getQueryResultsByCode(data.value)
          resultsF map {
            case Some(result) =>
              Ok(Json.prettyPrint(Json.toJson(result))).as("application/json")
            case None =>
              BadRequest(s"Not found ${data.value}")
          }
        }
    )
  }

  /**
    * A method for handling querying via name
    **/
  def queryByName() = Action.async { implicit request: Request[AnyContent] =>
    form.bindFromRequest.fold(
      errors =>{
        Future{
          BadRequest(s"Error while receiving from Form")
        }
      },
      data =>{
        val resultsF = reportService.getQueryResultsByName(data.value)
        resultsF map {
          case Some(result) =>
            Ok(Json.prettyPrint(Json.toJson(result))).as("application/json")
          case None =>
            BadRequest(s"Not found ${data.value}")
        }
      }
    )
  }

  val form = Form(
    mapping(
      "value" -> nonEmptyText
    )(Query.apply)(Query.unapply)
  )

  case class Query(value: String)

}

