package controllers

import javax.inject._

import data.QueryReportServiceI
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class ReportController @Inject()(cc: ControllerComponents, reportService:QueryReportServiceI) extends AbstractController(cc) {
  val maxResultsFromConfig = 10

  def report = Action.async{ implicit request: Request[AnyContent] =>
    val resultsF = reportService.getReport(maxResultsFromConfig)
    resultsF map {
        case Some(result) =>
          Ok(Json.prettyPrint(Json.toJson(result))).as(JSON)
        case None =>
          BadRequest("Got empty response from server")
    }
  }
}
