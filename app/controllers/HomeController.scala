package controllers


import javax.inject._

import data.DataSource
import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class HomeController @Inject()(cc: ControllerComponents, dataSource:DataSource) extends AbstractController(cc) {

  /**
    * Just rendering the home page. Fetch the country codes from the data source directly.
    **/
  def index = Action.async { implicit request:Request[AnyContent] =>
    val countryCodesF = dataSource.getCountryCodes
    countryCodesF map { countryCodes =>
      Ok(views.html.index(countryCodes))
    }
  }
}
