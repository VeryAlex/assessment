package modules

import com.google.inject.AbstractModule
import data.{DataSource, FileDataSource, QueryReportServiceI, QueryReportServiceImpl}
import play.api.{Configuration, Environment}

/**
  * Moduler for binding dependencies
  **/
class GuiceModule(environment: Environment, configuration: Configuration)
  extends AbstractModule {

  /**
    * Binding from traits to concrete implementations
    **/
  override def configure() = {
    bind(classOf[DataSource]).to(classOf[FileDataSource])
    bind(classOf[QueryReportServiceI]).to(classOf[QueryReportServiceImpl])
  }
}