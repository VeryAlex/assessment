package data

import javax.inject.Singleton

import models._

import scala.concurrent.Future

//Operations would be faster if we managed to populate on a database
@Singleton
class DatabaseDataSource extends DataSource{
  override def getCountries: Future[Seq[Country]] = ???

  override def getCountryCodes: Future[Seq[String]] = ???

  override def getCountry(partialName: String): Future[Option[Country]] = ???

  override def getCountryByCode(code: String): Future[Option[Country]] = ???


  override def getRunway(countryCode: String): Future[Seq[Runway]] = ???

  override def getAllRunways: Future[Seq[Runway]] = ???

  override def getAirports: Future[Seq[Airport]] = ???

  override def getAirportsByCountry(countryCode: String): Future[Seq[Airport]] = ???
}
