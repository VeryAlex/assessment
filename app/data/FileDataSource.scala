package data

import javax.inject.Singleton

import models._
import purecsv.safe.CSVReader

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * First kind of datasource. Reading directly from file and storing that information in memory.
  **/
@Singleton
class FileDataSource extends DataSource{

  /**
    * We are reading the CSV data directly with a CSV parsing library
    **/
  val Countries = {
    CSVReader[Country].readCSVFromFileName("conf/countries.csv",skipHeader = true)
      .filter( p => p.isSuccess)
      .map(p => p.get)
  }.sortBy(_.code)

  val Airports = {
    CSVReader[Airport].readCSVFromFileName("conf/airports.csv",skipHeader = true)
      .filter( p => p.isSuccess)
      .map(p => p.get)
  }.sortBy(_.isoCountry)

  val Runways = {
    CSVReader[Runway].readCSVFromFileName("conf/runways.csv",skipHeader = true)
      .filter( p => p.isSuccess)
      .map(p => p.get)
  }.sortBy(_.airportIdent)

  /**
    * Retrieval operations are quite straightforward. All of them run in a Future.
    **/
  
  override def getCountries = Future{Countries}

  override def getAllRunways: Future[Seq[Runway]] = Future{Runways}

  override def getCountryCodes = Future{Countries.map(c => c.code)}

  //I'm using a very dumb strategy: The partial name has to be prefix, if more than one country has same prefix only one is returned
  override def getCountry(partialName: String) = Future{Countries.find(c => c.name.startsWith(partialName))}

  override def getCountryByCode(code:String) = Future{Countries.find(c => c.code == code)}

  override def getAirportsByCountry(countryCode: String) = Future{Airports.filter(a => a.isoCountry == countryCode)}

  override def getRunway(countryCode: String) = {
    val airportsF = getAirportsByCountry(countryCode)
    airportsF flatMap {
      airports => {
        val airportIds = airports.map(a => a.idEnt)
        Future{
          Runways.filter(r => airportIds.contains(r.airportIdent))
        }
      }
    }
  }

  override def getAirports: Future[Seq[Airport]] = Future(Airports)
}
