package data

import models._
import scala.concurrent.Future

/**
  * Defines what we need from a data source.
  **/
trait DataSource{
  def getCountries:Future[Seq[Country]]
  def getAirports:Future[Seq[Airport]]
  def getCountryCodes:Future[Seq[String]]
  def getCountry(partialName:String):Future[Option[Country]]
  def getCountryByCode(code:String):Future[Option[Country]]
  def getAirportsByCountry(countryCode:String):Future[Seq[Airport]]
  def getRunway(countryCode:String):Future[Seq[Runway]]
  def getAllRunways:Future[Seq[Runway]]
}



