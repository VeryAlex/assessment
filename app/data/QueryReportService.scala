package data

import javax.inject.Inject

import models._

import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Define what a QueryReportService should do
  **/
trait QueryReportServiceI {
  def getQueryResultsByCode(code:String):Future[Option[CountryQuery]]
  def getQueryResultsByName(name:String):Future[Option[CountryQuery]]
  def getReport(results:Int):Future[Option[Report]]
}


/**
  * The QueryReportServiceImplementation requires a datasource and implements the QueryReportService trait
  **/
class QueryReportServiceImpl @Inject() (dataSource:DataSource) extends QueryReportServiceI{

  /**
    * Get a query from a given country. This definition is private, meant to used once we have a specific country
    **/
  private def getCountryQuery(country:Country):Future[Option[CountryQuery]] = {
    val airportsF = dataSource.getAirportsByCountry(country.code)
    val runwaysF = dataSource.getRunway(country.code)
    for(
      airport <- airportsF;
      runway <- runwaysF
    ) yield {
      Some(CountryQuery(country, airport, runway))
    }
  }

  /**
    * Get a CountryQuery from a country code
    **/
  override def getQueryResultsByCode(code:String) = {
    val countryF = dataSource.getCountryByCode(code)
    countryF flatMap {
      case Some(country) => getCountryQuery(country)
      case _ => Future.successful(None)
    }
  }

  /**
    * Get a CountryQuery from a name. The name can be partially complete
    **/
  override def getQueryResultsByName(name:String) = {
    val countryF = dataSource.getCountry(name)
    countryF flatMap {
      case Some(country) => getCountryQuery(country)
      case _ => Future.successful(None)
    }
  }

  /**
    * From the given sequence of countries and airports, return a CountryReport which contains a country with
    * its airport count.
    **/
  private def getCountryReportObjects(countries:Seq[Country], airports:Seq[Airport]) = {
    val sortedCountries = countries.sortBy(_.code)
    val sortedAirports = airports.sortBy(x => x.isoCountry).to[ListBuffer]

    val reports = for(
      country <- sortedCountries
    ) yield {
      //Since airports are sorted by country code, we can take them while the country code is equal
      val airportsPerCountry = sortedAirports.takeWhile(_.isoCountry == country.code)
      //We are removing from the ListBuffers in order to "skip" the first already processed elements
      sortedAirports --= airportsPerCountry
      CountryReport(country.id,country.code,country.name,country.continent,sortedAirports.size)
    }
    reports.toList
  }

  /**
    * From the given sequence of countries, airports and runways, return a CountrySurface which contains a country with
    * its unique surfaces.
    **/
  private def getSurfaces(countries:Seq[Country], airports:Seq[Airport],runways:Seq[Runway]) = {
    val sortedAirports = airports.sortBy(x => x.idEnt)
    val sortedRunways = runways.sortBy(_.airportIdent).to[ListBuffer]

    val runwaysWithCode = ( for(
      airport <- sortedAirports
    ) yield {

      val runwaysWithCountryCode = sortedRunways.takeWhile(_.airportIdent == airport.idEnt)

      sortedRunways --= runwaysWithCountryCode
      runwaysWithCountryCode.map((airport.isoCountry,_)) //We are yielding a List
    } ).flatten //So at the end we get a list of lists. We gotta flatten

    //Now we have the association : (country_code,runway), hence we group by country code
    val sortedRunwaysWithCode = runwaysWithCode.groupBy(_._1).withDefaultValue(Seq()) //A default value for countries without runways

    for(country <- countries) yield {
      //Just look for the entry in the map
      val uniqueSurfaces = sortedRunwaysWithCode(country.code).map(_._2).flatMap(_.surface).distinct
      CountrySurfaces(country.id,country.code,country.name,country.continent,uniqueSurfaces)
    }
  }



  /**
    * Get the n most common IDS from a list of runways
    **/
  private def getMostCommonIds(runways:Seq[Runway], n:Int) = {
    runways
      .flatMap(_.leIdent) //get leIdent from runways
      .groupBy(identity) //group them
      .mapValues(_.size) //get a new Map with value as the # of elements
      .toSeq //to seq in order to sort
      .sortBy(-_._2) //sort this new collection by # of elements in reverse order
      .take(n) //take the n elements
      .map(_._1) //and map to leIdent
  }

  /**
    * Create a Report object with
    *
    * n countries with the highest  and n lowest number of airports
    * unique surfaces per country
    * most common ids from all runways.
    **/
  override def getReport(n:Int) = {
    if(n < 1){
      Future(None) //nothing to return actually
    } else {

      val allCountriesF = dataSource.getCountries
      val allAirportsF = dataSource.getAirports
      val allRunwaysF = dataSource.getAllRunways

      for(
        countries <- allCountriesF;
        airports <- allAirportsF;
        runways <- allRunwaysF
      ) yield {

        val countryReports = getCountryReportObjects(countries,airports).sorted

        val lowest = countryReports.take(n)
        val highest = countryReports.reverse.take(n)

        val surfaces = getSurfaces(countries,airports,runways)

        val mostCommonIds = getMostCommonIds(runways,n)

        Some(Report(highest, lowest, surfaces, mostCommonIds))
      }
    }
  }
}
