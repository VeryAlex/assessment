package models

import play.api.libs.json.Json

case class CountryReport(id:Int,code:String,name:String
                         ,continent:String,airports:Int)

case class CountrySurfaces(id:Int,code:String,name:String
                           ,continent:String,uniqueSurfaces:Seq[String])

case class Report(highest:Seq[CountryReport],lowest:Seq[CountryReport],runwayTypes:Seq[CountrySurfaces],
                  mostComonRunwayIds:Seq[String])


object CountryReport{
  implicit val format = Json.format[CountryReport]

  implicit def orderingByName[A <: CountryReport]: Ordering[A] =
    Ordering.by(e => e.airports)
}

object CountrySurfaces{
  implicit val format = Json.format[CountrySurfaces]
}

object Report {
  implicit val format = Json.format[Report]
}