package models


/**
  * Representation of the CSV data provided.
  **/
case class Airport(id:Int,idEnt:String,airportType:String,name:String,
                   latituteDeg:Double,longitudDeg:Double,elevation:Option[Int],
                   continent:String,isoCountry:String,isoRegion:String,municipality:Option[String],
                   scheduledService:String,gpsCode:Option[String],iataCode:Option[String],
                   localCode:Option[String],homeLink:Option[String],wikiLink:Option[String],keywords:Option[String]
                   )

case class Country(id:Int,code:String,name:String,continent:String,wikiLink:String,keyWords:Option[String] = None)

case class Runway(id:Int,airportRef:Int,airportIdent:String,lengthFt:Option[Int],
                  widthFt:Option[Int],surface:Option[String],lighted:Boolean,closed:Boolean,
                  leIdent:Option[String],leLatitudeDeg:Option[Double],leLongitudeDeg:Option[Double],
                  leElevationFt:Option[Int],leHeadingDegT:Option[Double],leDisplacedThresholdFt:Option[Int],
                  heIdent:Option[String],heLatitudeDeg:Option[Double],heLongitudeDeg:Option[Double],
                  heElevationFt:Option[Int],heHeadingDegT:Option[Double],heDisplacedThresholdFt:Option[Int]
                 )
