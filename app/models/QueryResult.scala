package models

import play.api.libs.json.Json

/**
  * Composition of objects for structuring a Json response.
  **/
case class RunwayQuery(id:Int,lengthFt:Option[Int],widthFt:Option[Int],surface:Option[String],leIdent:Option[String])

case class AirportQuery(id:Int,idEnt:String,airportType:String,name:String,
                         latituteDeg:Double,longitudDeg:Double,elevation:Option[Int],
                         runways:Seq[RunwayQuery])


case class CountryQuery(id:Int,code:String,name:String
                         ,continent:String,airports:Seq[AirportQuery])




object RunwayQuery{
  implicit val format = Json.format[RunwayQuery]
  def apply(runwayData:Runway):RunwayQuery = {
    RunwayQuery(runwayData.id,runwayData.lengthFt,runwayData.widthFt,runwayData.surface,runwayData.leIdent)
  }
}

object AirportQuery{
  implicit val format = Json.format[AirportQuery]

  def apply(airportData:Airport,runwayData:Seq[Runway]):AirportQuery ={
    val runways = runwayData.map(runway => RunwayQuery(runway))
    AirportQuery(airportData.id,airportData.idEnt,airportData.airportType,
      airportData.name,airportData.latituteDeg,airportData.longitudDeg,airportData.elevation,
      runways)
  }
}

object CountryQuery{
  implicit val format = Json.format[CountryQuery]

  def apply(countryData:Country,airportData:Seq[Airport], runwayData:Seq[Runway]):CountryQuery = {
    val airports = airportData.map(airport => AirportQuery(airport,runwayData))
    CountryQuery(countryData.id,countryData.code,countryData.name,countryData.continent,airports)
  }

}
