# My project's README

This is a web application that asks the user for two actions : Query or Reports.

* Query Option asks the user for country name or code and prints the airports & runways at each airport.
The name can be partial:  if you input a prefix of the country you are looking for you will get a result e.g. Mex will return results for Mexico if more than one country has the same prefix, only the first result is returned.

* Reports option prints the following:

    * 10 countries with highest number of airports (with count) and countries with lowest number of airports.
    * Unique types of runways for all countries.
    * The top 10 most common runway identifications from all runways.



### Execute tests

Run

```
$ sbt test
```

### Run application in dev

```
$ sbt run
```

the application will start at http://localhost:9000 by default.

### Run application in production

Generate a new application secret and save it somewhere
```
$ sbt playGenerateSecret
```

Create the distribution zip
```
$ sbt dist
```

Copy and extract the zip in the server and change the permission to the script
```
$ scp path/to/the/distzip/server-version.zip root@server:/path/to/the/app/
$ ssh roo@server
$ cd /path/to/the/app/
$ unzip server-version.zip
$ cd server-version/bin
$ chmod 755 server
```

Start the server as background process using the application secreted generated earlier
```
nohup ./server -Dplay.crypto.secret=<SECRET>" -Dhttp.port=<PORT> &
```

//TODO:  Populate views.
