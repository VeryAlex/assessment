
name := "assessment"
organization := "personal"

version := "1.0-SNAPSHOT"
scalaVersion := "2.12.2"

lazy val root = (project in file(".")).enablePlugins(PlayScala)
resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases"

)

libraryDependencies ++= Seq(
  guice,
  "com.github.melrief" %% "purecsv" % "0.1.0",
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.1" % "test",
  "org.mockito" % "mockito-all" % "1.10.19"% "test"
)

coverageMinimum := 60
coverageFailOnMinimum := true



